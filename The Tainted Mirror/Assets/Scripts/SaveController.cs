﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.SceneManagement;

public class SaveController : MonoBehaviour {


	// Use this for initialization
	void Start () {
	}

    public void CheckpointSaver(Vector3 pos, PlayerController player, int levelNum)
    {

        //create xml doc
        XmlDocument Save = new XmlDocument();
        XmlElement rootElement = Save.CreateElement("Save");

        Save.AppendChild(rootElement);

        //Create element
        XmlElement Level = Save.CreateElement("Level");

        //Create attributes
        XmlAttribute LevelNum = Save.CreateAttribute("Num");

        //Assign Values
        LevelNum.Value = levelNum.ToString();

        //Add Values
        Level.Attributes.Append(LevelNum);

        //Append Element
        rootElement.AppendChild(Level);

        //Create element
        XmlElement SavePoint = Save.CreateElement("SavePoint");

        //Create attributes
        XmlAttribute XPoint = Save.CreateAttribute("X");
        XmlAttribute YPoint = Save.CreateAttribute("Y");
        XmlAttribute ZPoint = Save.CreateAttribute("Z");

        //Assing Values 
        XPoint.Value = pos.x.ToString();
        YPoint.Value = pos.y.ToString();
        ZPoint.Value = pos.z.ToString();

        //Add Values 
        SavePoint.Attributes.Append(XPoint);
        SavePoint.Attributes.Append(YPoint);
        SavePoint.Attributes.Append(ZPoint);

        //Append element
        rootElement.AppendChild(SavePoint);

        //Create element
        XmlElement Keys = Save.CreateElement("Keys");

        //create attributes
        XmlAttribute KeyNum = Save.CreateAttribute("KeyAmount");

        //Assign Values
        KeyNum.Value = player.GetKeyNum().ToString();

        //Add Values
        Keys.Attributes.Append(KeyNum);

        for (int i = 0; i < player.keys.Count; i++)
        {
            XmlElement keyElem = Save.CreateElement("key");
            Keys.AppendChild(keyElem);

            keyElem.Attributes.Append(Save.CreateAttribute("type"));
            keyElem.Attributes.Append(Save.CreateAttribute("image"));
            keyElem.Attributes.Append(Save.CreateAttribute("color"));

            keyElem.Attributes["type"].Value = player.keys[i].type.ToString();
            keyElem.Attributes["image"].Value = player.keys[i].image.name;
            keyElem.Attributes["color"].Value = player.keys[i].keyColor.ToString();
        }

        //Append element
        rootElement.AppendChild(Keys);

        //Save the xml
        Save.Save("SaveGame.Xml");
    }

    public void AudioSaver()
    {
        AudioController audioSettings = GetComponent<AudioController>();

        XmlDocument Audio = new XmlDocument();
        XmlElement rootElem = Audio.CreateElement("AudioSettings");

        Audio.AppendChild(rootElem);

        //Effects Slider
        XmlElement Effects = Audio.CreateElement("Effects");

        XmlAttribute VolumeE = Audio.CreateAttribute("Volume");

        VolumeE.Value = audioSettings.EffectsSlider.value.ToString();

        Effects.Attributes.Append(VolumeE);

        rootElem.AppendChild(Effects);

        //Music Slider
        XmlElement Music = Audio.CreateElement("Music");

        XmlAttribute VolumeM = Audio.CreateAttribute("Volume");

        VolumeM.Value = audioSettings.MusicSlider.value.ToString();

        Music.Attributes.Append(VolumeM);

        rootElem.AppendChild(Music);

        //Mute button Status

        XmlElement Mute = Audio.CreateElement("Mute");

        XmlAttribute Status = Audio.CreateAttribute("Status");

        Status.Value = audioSettings.muteStatus.ToString();

        rootElem.AppendChild(Mute);

        Audio.Save("AudioSettings.Xml");
    }

    public void controlsSaver()
    {
        InputManager input = GetComponent<InputManager>();

        XmlDocument controls = new XmlDocument();
        XmlElement rootElem = controls.CreateElement("ControlsSettings");

        controls.AppendChild(rootElem);

        //Jump Key
        XmlElement JumpKey = controls.CreateElement("Jump");

        XmlAttribute Jump = controls.CreateAttribute("Key");

        Jump.Value = input.GetKeyCode("Jump").ToString();

        rootElem.AppendChild(JumpKey);

        //crouch Key
        XmlElement CrouchKey = controls.CreateElement("Crouch");

        XmlAttribute Crouch = controls.CreateAttribute("Key");

        Crouch.Value = input.GetKeyCode("Jump").ToString();

        rootElem.AppendChild(CrouchKey);

        //interact Key
        XmlElement InteractKey = controls.CreateElement("Interact");

        XmlAttribute Interact = controls.CreateAttribute("Key");

        Interact.Value = input.GetKeyCode("Interact").ToString();

        rootElem.AppendChild(InteractKey);

        //Up Key
        XmlElement UpKey = controls.CreateElement("Up");

        XmlAttribute Up = controls.CreateAttribute("Key");

        Up.Value = input.GetKeyCode("Up").ToString();

        rootElem.AppendChild(UpKey);

        //Down Key
        XmlElement DownKey = controls.CreateElement("Down");

        XmlAttribute Down = controls.CreateAttribute("Key");

        Down.Value = input.GetKeyCode("Down").ToString();

        rootElem.AppendChild(DownKey);

        //left Key
        XmlElement LeftKey = controls.CreateElement("Left");

        XmlAttribute Left = controls.CreateAttribute("Key");

        Left.Value = input.GetKeyCode("Left").ToString();

        rootElem.AppendChild(LeftKey);

        //Right Key
        XmlElement RightKey = controls.CreateElement("Right");

        XmlAttribute Right = controls.CreateAttribute("Key");

        Right.Value = input.GetKeyCode("Right").ToString();

        rootElem.AppendChild(RightKey);

        //Alt Up Key
        XmlElement AltUpKey = controls.CreateElement("AltUp");

        XmlAttribute AltUp = controls.CreateAttribute("Key");

        AltUp.Value = input.GetKeyCode("Alt Up").ToString();

        rootElem.AppendChild(AltUpKey);

        //Alt Down Key
        XmlElement AltDownKey = controls.CreateElement("AltDown");

        XmlAttribute AltDown = controls.CreateAttribute("Key");

        AltDown.Value = input.GetKeyCode("Alt Down").ToString();

        rootElem.AppendChild(AltDownKey);

        //Alt Left Key
        XmlElement AltLeftKey = controls.CreateElement("AltLeft");

        XmlAttribute AltLeft = controls.CreateAttribute("Key");

        AltLeft.Value = input.GetKeyCode("Alt Left").ToString();

        rootElem.AppendChild(AltLeftKey);

        //Alt Right Key
        XmlElement AltRightKey = controls.CreateElement("AltRight");

        XmlAttribute AltRight = controls.CreateAttribute("Key");

        AltRight.Value = input.GetKeyCode("Alt Right").ToString();

        rootElem.AppendChild(AltRightKey);

        //Run Key
        XmlElement RunKey = controls.CreateElement("Run");

        XmlAttribute Run = controls.CreateAttribute("Key");

        Run.Value = input.GetKeyCode("Run").ToString();

        rootElem.AppendChild(RunKey);

        //Save File
        controls.Save("ControlsSettings.Xml");
    }

    public void CheckpointLoader(string filename, PlayerController player)
    {
        // TODO: call this function when you want to load the xml data from file.

        XmlDocument doc = new XmlDocument();
        doc.Load(filename);

        XmlElement rootElem = doc.DocumentElement; // get the root note "Save"

        XmlElement levelElem = rootElem.GetElementsByTagName("Level")[0] as XmlElement;
        XmlElement savePointElem = rootElem.GetElementsByTagName("SavePoint")[0] as XmlElement;
        XmlElement keysElem = rootElem.GetElementsByTagName("keys")[0] as XmlElement;

        int levelNum = int.Parse(levelElem.Value);

        // update every time a new level is added
        switch (levelNum)
        {
            case 1:
                SceneManager.LoadScene("Level Scene");
                break;
        }

        player.RespawnPos = new Vector3(float.Parse(savePointElem.GetAttribute("X")),
            float.Parse(savePointElem.GetAttribute("Y")),
            float.Parse(savePointElem.GetAttribute("Z")));

        XmlElement keyElem = keysElem.FirstChild as XmlElement;
        while (keyElem != null)
        {
            KeyData keyData = new KeyData();

            keyData.type = (KeyData.EType)System.Enum.Parse(typeof(KeyData.EType), keyElem.Attributes["type"].Value);

            GameObject[] shards = GameObject.FindGameObjectsWithTag("CheckPoint");
            GameObject[] images = GameObject.FindGameObjectsWithTag("InvImage");

            for (int i = 0; i < player.keys.Count; i++)
            {
                if (shards[i].name == keyElem.Attributes["colour"].ToString())
                {
                    keyData.keyColor = shards[i].GetComponent<Color>();
                    Destroy(shards[i]);
                }

                if (images[i].name == keyElem.Attributes["image"].ToString())
                {
                    keyData.image = images[i].GetComponent<Image>();
                }
            }

            player.AddKey(keyData); // adding the loaded key data into the passed in player

            keyElem = keyElem.NextSibling as XmlElement;
        }

    }

    public void AudioLoader(string filename, AudioController audio)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filename);

        XmlElement rootElement = doc.DocumentElement;

        XmlElement EffectElem = rootElement.GetElementsByTagName("Effects")[0] as XmlElement;
        XmlElement MusicElem = rootElement.GetElementsByTagName("Music")[0] as XmlElement;
        XmlElement MuteElem = rootElement.GetElementsByTagName("Mute")[0] as XmlElement;

        audio.EffectsSlider.value = float.Parse(EffectElem.Value);
        audio.MusicSlider.value = float.Parse(MusicElem.Value);
        audio.muteStatus = bool.Parse(MuteElem.Value);
    }

    public void ControlLoader(string filename, InputManager input)
    {
        XmlDocument doc = new XmlDocument();
        doc.Load(filename);

        XmlElement rootElement = doc.DocumentElement;

        XmlElement jumpElem = rootElement.GetElementsByTagName("JumpKey")[0] as XmlElement;
        XmlElement crouchElem = rootElement.GetElementsByTagName("CrouchKey")[0] as XmlElement;
        XmlElement interactElem = rootElement.GetElementsByTagName("InteractKey")[0] as XmlElement;
        XmlElement upElem = rootElement.GetElementsByTagName("UpKey")[0] as XmlElement;
        XmlElement downElem = rootElement.GetElementsByTagName("DownKey")[0] as XmlElement;
        XmlElement leftElem = rootElement.GetElementsByTagName("LeftKey")[0] as XmlElement;
        XmlElement rightElem = rootElement.GetElementsByTagName("RightKey")[0] as XmlElement;
        XmlElement altUpElem = rootElement.GetElementsByTagName("AltUpKey")[0] as XmlElement;
        XmlElement altDownElem = rootElement.GetElementsByTagName("AltDownKey")[0] as XmlElement;
        XmlElement altLeftElem = rootElement.GetElementsByTagName("AltLeftKey")[0] as XmlElement;
        XmlElement altRightElem = rootElement.GetElementsByTagName("AltRightKey")[0] as XmlElement;
        XmlElement runElem = rootElement.GetElementsByTagName("RunKey")[0] as XmlElement;

        input.SetKeyMap("Jump", (KeyCode) System.Enum.Parse(typeof(KeyCode), jumpElem.Attributes["JumpKey"].Value));
        input.SetKeyMap("Crouch", (KeyCode)System.Enum.Parse(typeof(KeyCode), crouchElem.Attributes["CrouchKey"].Value));
        input.SetKeyMap("Interact", (KeyCode)System.Enum.Parse(typeof(KeyCode), interactElem.Attributes["InteractKey"].Value));
        input.SetKeyMap("Up", (KeyCode)System.Enum.Parse(typeof(KeyCode), upElem.Attributes["UpKey"].Value));
        input.SetKeyMap("Down", (KeyCode)System.Enum.Parse(typeof(KeyCode), downElem.Attributes["DownKey"].Value));
        input.SetKeyMap("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), leftElem.Attributes["LeftKey"].Value));
        input.SetKeyMap("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), rightElem.Attributes["RightKey"].Value));
        input.SetKeyMap("Alt Up", (KeyCode)System.Enum.Parse(typeof(KeyCode), altUpElem.Attributes["AltUpKey"].Value));
        input.SetKeyMap("Alt Down", (KeyCode)System.Enum.Parse(typeof(KeyCode), altDownElem.Attributes["AltDownKey"].Value));
        input.SetKeyMap("Alt Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), altLeftElem.Attributes["AltLeftKey"].Value));
        input.SetKeyMap("Alt Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), altRightElem.Attributes["AltRightKey"].Value));
        input.SetKeyMap("Run", (KeyCode)System.Enum.Parse(typeof(KeyCode), runElem.Attributes["RunKey"].Value));
    }
}