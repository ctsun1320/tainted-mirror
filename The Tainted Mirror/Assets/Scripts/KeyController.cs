﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyController : MonoBehaviour
{
    public KeyData keyData;

    void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        UIHandler UIMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();
        //Player2Controller player = other.GetComponent<Player2Controller>();
        SaveController save = GetComponent<SaveController>();

        if (player != null)
        {
            player.AddKey(keyData);
            Destroy(this.gameObject);
            save.CheckpointSaver(player.RespawnPos, player, UIMenu.level);
        }
    }
}

[System.Serializable]
public class KeyData
{
    public enum EType
    {
        DOOR_DECOY_KEY,
        DOOR_KEY,
    }

    public EType type;
    public SpriteRenderer shardSprite;
    public Color keyColor;
    public Image image;
}