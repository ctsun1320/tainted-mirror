﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour {

    public void PlayButtonClick()
    {
        SceneManager.LoadScene("Level Scene");
    }

    public void QuitButtonClick()
    {
        Application.Quit();
    }
}
