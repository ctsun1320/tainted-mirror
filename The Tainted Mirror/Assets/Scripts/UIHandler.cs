﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIHandler : MonoBehaviour {

    //public PlayerController player;

    public bool gameOver;
    public Canvas pauseMenu;
    public Canvas DialougeUI;

    public Canvas DeathScreen;

    public Canvas SettingsMenu;
    public Canvas ControlsSettings;
    public Canvas VolumeSettings;

    public bool isPaused;
    public bool wasActivated = false;
    public GameObject player;

    public int level = 0;

    public Transform[] spawnSpots;

    public Transform[] enemySpawnSpots;

    public GameObject[] enemyTriggers;

    [SerializeField] private List<Vector3> destroyedPlatforms;

    [SerializeField] private TextBoxManager textbox;

    // Use this for initialization
    void Start()
    {
        //if (player == null)
        //    player = GameObject.FindGameObjectWithTag("Player");
        destroyedPlatforms = new List<Vector3>();

        DontDestroyOnLoad(this.gameObject); //don't blow up the game ui when loading a new level - might need a destroy call when quitting to menu, though
        if (SettingsMenu != null)
            SettingsMenu.enabled = false;
        if (ControlsSettings != null)
            ControlsSettings.enabled = false;
        if (VolumeSettings != null)
            VolumeSettings.enabled = false;
        if (DialougeUI != null)
            DialougeUI.enabled = false;

        if (DeathScreen != null)
            DeathScreen.enabled = false;

        if (pauseMenu != null)
            pauseMenu.enabled = false;
        isPaused = false;

        textbox = FindObjectOfType<TextBoxManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && DialougeUI != null && DialougeUI.enabled == false)
        {
            if (SettingsMenu.enabled == true)
            {
                SettingsBackButtonClick();
            }
            else if (ControlsSettings.enabled == true)
            {
                ControlsBackButtonClick();
            }
            else if (VolumeSettings.enabled == true)
            {
                VolumeBackButtonClick();
            }
            else
                PauseGame();
        }

        if (!gameOver)
        {
            if (player == null) //if this has no reference to a player, eg. when loading a new scene
            {
                GameObject[] playerObjs = GameObject.FindGameObjectsWithTag("Player");//tries to find a player object already in scene
                if (playerObjs.Length == 0) //if it can't be found
                {
                    player = GameObject.Instantiate(Resources.Load("Player")) as GameObject; //spawns one

                    if (spawnSpots.Length > 0)
                    {
                        player.transform.position = spawnSpots[0].position;
                    }
                    else
                    {
                        GameObject[] playerSpawns = GameObject.FindGameObjectsWithTag("PlayerSpawn");
                        Transform first = null;
                        foreach (GameObject obj in playerSpawns)
                        {
                            if (first == null)
                                first = obj.transform;
                            else //go for the leftmost as the first spawnSpot
                            {
                                if (obj.transform.position.x < first.position.x)
                                {
                                    first = obj.transform;
                                }
                            }
                        }
                        player.transform.position = first.position;
                    }
                }
                else if (playerObjs.Length > 0) //if there are already player objects 
                {
                    if (playerObjs.Length == 1) //if there is only one player object
                    {
                        player = playerObjs[0]; //sets the reference to that
                    }
                    else if (playerObjs.Length > 1) //if there are more than one
                    {
                        //find the closest to the PlayerSpawn object (or the first playerSpawn object, if list is defined)
                        Transform playerSpawn;
                        if (spawnSpots.Length > 0)
                        {
                            playerSpawn = spawnSpots[0];
                        }
                        else //if order hasn't been defined, go for the leftmost one
                        {
                            GameObject[] playerSpawns = GameObject.FindGameObjectsWithTag("PlayerSpawn");
                            Transform first = null;
                            foreach (GameObject obj in playerSpawns)
                            {
                                if (first == null)
                                    first = obj.transform;
                                else //go for the leftmost as the first spawnSpot
                                {
                                    if (obj.transform.position.x < first.position.x)
                                    {
                                        first = obj.transform;
                                    }
                                }
                            }
                            playerSpawn = first;
                        }
                        GameObject closestPlayer = null; //sets closet player object to null to start with
                        foreach (GameObject obj in playerObjs) //go through the found player objects
                        {
                            if (closestPlayer == null)
                                closestPlayer = obj;
                            else
                            {  //and tries to find the closest object to the PlayerSpawn object
                                if (Vector3.Distance(playerSpawn.position, obj.transform.position)
                                    < Vector3.Distance(playerSpawn.position, closestPlayer.transform.position))
                                    closestPlayer = obj;
                            }
                        }
                        player = closestPlayer; //sets the reference to that object

                        for (int i = 0; i < playerObjs.Length; i++) //go through the rest and destroy them
                        {
                            if (playerObjs[i] != player)
                                GameObject.DestroyObject(playerObjs[i]);
                        }
                    }
                }
                CameraController camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
                camera.target = player; //slaves the camera to that
                camera.offset = new Vector3(0, 0, 5);
                if (destroyedPlatforms.Count > 0)
                {
                    destroyedPlatforms.Clear(); //clear the destroyed platforms list. it's a new scene now.
                }
            }
        }
 
    }

    public void ResumeButtonClick()
    {
        PauseGame();
    }

    public void SettingsButtonClick()
    {
        SettingsMenu.enabled = true;
        pauseMenu.enabled = false;
    }

    public void SettingsBackButtonClick()
    {
            SettingsMenu.enabled = false;
            pauseMenu.enabled = true;
    }

    public void QuitButtonClick()
    {
		SceneManager.LoadScene("Main Menu");
		GameObject.DestroyObject(this.gameObject); //no need for a game ui on the main menu
		Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f); //unpause the game now that we are going back to main menu
    }

    public void VolumeButtonClick()
    {
        SettingsMenu.enabled = false;
        VolumeSettings.enabled = true;
    }

    public void VolumeBackButtonClick()
    {
        SettingsMenu.enabled = true;
        VolumeSettings.enabled = false;

    }

    public void ControlsButtonClick()
    {
        SettingsMenu.enabled = false;
        ControlsSettings.enabled = true;
    }

    public void ControlsBackButtonClick()
    {
        SettingsMenu.enabled = true;
        ControlsSettings.enabled = false;
    }

    public void PauseGame()
    {
        pauseMenu.enabled = !pauseMenu.enabled;
        isPaused = !isPaused;
        Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
    }

    public void DialougeUIHandler()
    {

        DialougeUI.enabled = !DialougeUI.enabled;
        isPaused = !isPaused;
        textbox.isActive = !textbox.isActive;
        Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
    }

    public void RespawnButtonClick()
    {
        PlayerController playerCon = player.GetComponent<PlayerController>();
        if (playerCon.willDie)
        {
            playerCon.rb.velocity = new Vector3(0, 0, 0); //resets velocity - clears falling damage
            player.transform.position = playerCon.RespawnPos;
            playerCon.respawned = true;
            playerCon.rb.useGravity = true;
            playerCon.willDie = false;
            playerCon.JumpKeyPressed = false;

            //if there are any enemies on the screen, destroy them
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject obj in enemies)
            {
                GameObject.DestroyObject(obj);
            }

            foreach (GameObject obj in enemyTriggers) //re enable spawn for enemies
            {
                obj.GetComponent<EnemySpawner>().enemySpawned = false;
            }

            if (destroyedPlatforms.Count > 0)
            {
                foreach (Vector3 pos in destroyedPlatforms)
                {
                    GameObject platform = GameObject.Instantiate(Resources.Load("Platform (Small) (Falling)")) as GameObject;
                    platform.transform.position = pos;
                    //platform.GetComponent<PlatformController>().ResetPlat();
                }
                destroyedPlatforms.Clear();
            }
            Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
            isPaused = !isPaused;
            DeathScreen.enabled = false;
            wasActivated = false;
        }

    }

    public void AddPlatform(Vector3 destroyedPos)
    {
        destroyedPlatforms.Add(destroyedPos);
    }
}
