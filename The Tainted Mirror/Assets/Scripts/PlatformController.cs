﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour {

	private float timeLeft;
    public float maxTimeToFall = 1.0f;
    private float despawnTime = 0.1f;
    [SerializeField] private Animator animator;

    [SerializeField] private float Speed = 1.1f, distance;

    float accumulatedTime = 0.0f;

    //private bool timerStart;
    public bool PlayerOnPlat, despawnTimerStart, isBreaking;

    public enum Direction
    {
        NORMAL,
        REVERSED
    }

    public Direction dir;

    private Vector3 curPos;

    private Rigidbody rigid;
    //public Player2Controller player;
    public PlayerController player;

    [SerializeField] private UIHandler UIMenu;
    //[SerializeField] private RespawnController respawn;

    private void Awake()
    {
        if (UIMenu == null)
            UIMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();

        //if (respawn == null)
        //    respawn = GameObject.FindGameObjectWithTag("DeathPoint").GetComponent<RespawnController>();
    }
    void Start()
    {
        if (animator == null && this.gameObject.tag == "Falling")
            animator = gameObject.GetComponentInChildren<Animator>();
        curPos = new Vector3(transform.position.x, transform.position.y, 0);
        rigid = GetComponent<Rigidbody>();
        //timerStart = false;
        PlayerOnPlat = false;
        isBreaking = false;
		timeLeft = maxTimeToFall;
    }

	// Update is called once per frame
	void Update ()
    {

        if (player == null && UIMenu.player != null)// if this has no instance of playerController and UI has an instance
        { //make a reference
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>(); //moved to here due to possibility of player not in scene on awake.
            //player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player2Controller>();
        }


        accumulatedTime += Time.deltaTime;

        if (!UIMenu.isPaused)
        {
            if (this.tag == "Moving")
            {
                if (dir == Direction.NORMAL) // right to left
                {
                    //character.transform.position = new Vector3(this.gameObject.transform.position.x, character.transform.position.y, character.transform.position.z);
                    this.transform.Translate(transform.right * Mathf.Cos(accumulatedTime * Speed) * distance);

                }
                else if (dir == Direction.REVERSED) // left to right
                {
                    this.transform.Translate(transform.right * -Mathf.Cos(accumulatedTime * Speed) * distance);
                }
            }
        
            if (PlayerOnPlat == true || isBreaking == true) //(&& timerStart == true) - removed
            {
                timeLeft -= Time.deltaTime;
            }


            if (despawnTimerStart == true)
            {
                despawnTime -= Time.deltaTime;
            }
        }

        if (animator != null)
        {
            if (!UIMenu.isPaused)
            {
                if (animator.enabled == false)
                    animator.enabled = true;
            }
            else
            {
                if (animator.enabled == true)
                    animator.enabled = false;
            }
        }

        if (timeLeft < 1.00f && isBreaking == false) //this is timed to animation
        {
            if (animator != null)
                animator.SetBool("isBreaking", true);
            isBreaking = true;
        }
        if (timeLeft < 0)
        {
            CollapsePlatform();
        }

        if (despawnTime <= 0 && player.respawned == true)
            {
                ResetPlat();
            }

 
    }
    private void OnTriggerEnter(Collider other)
    {
        if (this.tag == "Falling" && other.tag == "Player")
        {
            //timerStart = true;
            PlayerOnPlat = true;
            player.respawned = false;
        }
        if (this.tag == "Falling" && other.tag != "Player" && other.tag != "Enemy" && other.tag != "DeathPoint" && this.timeLeft < 0) //if it's falling and making contact with something that's not the player or the enemy
        {
            //if (animator != null)
            //    animator.SetBool("isBreaking", false);
            //this.despawnTimerStart = true;
            UIMenu.AddPlatform(curPos);
            GameObject.DestroyObject(this.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (this.tag == "Falling" && other.tag == "Player")
        {
            PlayerOnPlat = false;
        }
        else if (this.tag == "Moving" && other.tag == "Player")
        {
            other.transform.parent = null;
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (this.tag == "Falling")
    //    {
    //        timerStart = true;
    //        PlayerOnPlat = true;
    //        player.respawned = false;
    //    }


    //}

    //private void OnTriggerEnter(Collider other)
    //{

    //    if (this.tag == "Moving")
    //    {
    //        other.gameObject.transform.parent = this.transform;
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (this.tag == "Moving")
    //    {
    //        if (other.gameObject.transform.IsChildOf(this.transform))
    //        {
    //            this.transform.DetachChildren();
    //        }
    //    }
    //}

    private void OnTriggerStay(Collider other)
    {
        //collision.collider.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);

        //if (this.tag == "Moving")
        //{
        //    collision.collider.transform.Translate(transform.right * Mathf.Cos(Time.time * 1.1f) * Time.deltaTime);

        //}

        //GameObject character = other.gameObject;

        if (this.tag == "Moving" && other.tag == "Player")
        {
            other.transform.parent = this.transform;

            //if (dir == Direction.NORMAL)
            //{
            //    //character.transform.position = new Vector3(this.gameObject.transform.position.x, character.transform.position.y, character.transform.position.z);
            //    other.transform.Translate(transform.right * Mathf.Cos(accumulatedTime * Speed) * distance);
            //}
            //else if (dir == Direction.REVERSED)
            //{
            //    other.transform.Translate(transform.right * -Mathf.Cos(accumulatedTime * Speed) * distance);
            //}
            //player.isJumping = false;
        }

    }

    private void CollapsePlatform()
    {
        rigid.useGravity = true;
        //timerStart = false;
        //timeLeft = 2.0f;
        isBreaking = false;
    }

    public void ResetPlat()
    {
        if (animator != null)
            animator.SetBool("isBreaking", false);
        isBreaking = false;
        rigid.MovePosition(curPos);
        rigid.useGravity = false;
        timeLeft = maxTimeToFall;
        //timerStart = false;
        rigid.velocity = new Vector3(0, 0, 0);
        PlayerOnPlat = false;
        despawnTime = 0.1f;
        despawnTimerStart = false;
        player.respawned = false;
    }
}
