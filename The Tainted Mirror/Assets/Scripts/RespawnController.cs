﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : SaveController
{
    [SerializeField] private UIHandler UIMenu;

    private void Start()
    {

        UIMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();
    }
    private void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        PlatformController plat = other.GetComponent<PlatformController>();
        

        if (gameObject.tag == "DeathPoint")
        {
            if (other.tag == "Player")
            {
                Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
                UIMenu.DeathScreen.enabled = true;
                UIMenu.isPaused = true;
                player.willDie = true;
                player.rb.useGravity = false;
                player.isJumping = false;
                player.JumpKeyPressed = false;

                GameObject[] fallingPlatforms = GameObject.FindGameObjectsWithTag("Falling");
                foreach (GameObject obj in fallingPlatforms)
                {
                    obj.GetComponent<PlatformController>().ResetPlat(); //resets every timer in all falling platforms if player dies.
                }
            }
            else if (other.tag == "Falling")
            {
                if (other.gameObject.GetComponentInChildren<Animator>() != null)
                    other.gameObject.GetComponentInChildren<Animator>().SetBool("isBreaking", false);
                plat.despawnTimerStart = true;

            }
        }
        else if (gameObject.tag == "CheckPoint" && other.tag == "Player") //only respond if the player pick up the checkpoint
        {
            player.RespawnPos = player.transform.position;

        }
    }
}
