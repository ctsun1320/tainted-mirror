﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour {

    private Dictionary<string, KeyCode> keyMapping;
    public string[] keyMaps = new string[12]
    {
        "Jump",
        "Crouch",
        "Interact",
        "Up",
        "Down",
        "Left",
        "Right",
        "Alt Up",
        "Alt Down",
        "Alt Left",
        "Alt Right",
        "Run"
    };
    public KeyCode[] defaultKeys = new KeyCode[12]
    {
        KeyCode.Space,
        KeyCode.LeftControl,
        KeyCode.E,
        KeyCode.W,
        KeyCode.S,
        KeyCode.A,
        KeyCode.D,
        KeyCode.UpArrow,
        KeyCode.DownArrow,
        KeyCode.LeftArrow,
        KeyCode.RightArrow,
        KeyCode.LeftShift
    };

    // Use this for initialization
    void Awake()
    {

        keyMapping = new Dictionary<string, KeyCode>();
        InitializeKeyMaps();
    }

    //keymaps
    private void InitializeKeyMaps()
    {
        for (int i = 0; i < keyMaps.Length; i++)
        {
            keyMapping.Add(keyMaps[i], defaultKeys[i]);
        }
    }

    public void SetKeyMap(string keyMap, KeyCode key)
    {
        if (!keyMapping.ContainsKey(keyMap))
            throw new ArgumentException("Invalid KeyMap for " + keyMap);
        foreach (KeyValuePair<string, KeyCode> kvp in keyMapping)//if the keymaps has the requested key but is not at the desired keyMap,
        {
            if (kvp.Value == key && kvp.Key != keyMap)
                return;
        }//return - do nothing - or give error or something to stop one key being mapped to two things

        keyMapping[keyMap] = key;
    }
    public bool GetKey(string keyMap)
    {
        return Input.GetKey(keyMapping[keyMap]);
    }
    public bool GetKeyDown(string keyMap)
    {
        return Input.GetKeyDown(keyMapping[keyMap]);
    }
    public KeyCode GetKeyCode(string keyMap)
    {
        return keyMapping[keyMap];
    }

}
