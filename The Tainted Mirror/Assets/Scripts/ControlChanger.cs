﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlChanger : MonoBehaviour {

    private Transform menuPanel;
    private Event keyEvent;
    private Text buttonText;
    private KeyCode newKey;
    private bool waitingForKey;

    [SerializeField] private InputManager input;

    public Button[] buttons;

    [SerializeField] private string[] keymaps;
    [SerializeField] private string[] buttonNames;

    // Use this for initialization

    private void Awake()
    {
        keymaps = new string[12]
        {
            "Jump",
            "Crouch",
            "Interact",
            "Up",
            "Down",
            "Left",
            "Right",
            "Alt Up",
            "Alt Down",
            "Alt Left",
            "Alt Right",
            "Run"
        };
        buttonNames = new string[12]
        {
            "jumpKey",
            "crawlKey",
            "interactKey",
            "upKey",
            "downKey",
            "leftKey",
            "rightKey",
            "altUpKey",
            "altDownKey",
            "altLeftKey",
            "altRightKey",
            "runKey",
        };
    }
    void Start()
    {
        menuPanel = this.gameObject.transform;
        waitingForKey = false;
        if (input == null)
            input = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<InputManager>();

        buttons = menuPanel.GetComponentsInChildren<Button>();
        
        foreach (Button button in buttons)
        {
            for (int i = 0; i < buttonNames.Length; i++)
            {
                if (button.name == buttonNames[i])
                {
                    button.GetComponentInChildren<Text>().text = input.GetKeyCode(keymaps[i]).ToString();
                }
            }
 
        }

    }

    private void OnGUI()
    {
        keyEvent = Event.current;
        if (keyEvent.isKey && waitingForKey)
        {
            newKey = keyEvent.keyCode;
            waitingForKey = false;
        }
    }

    public void StartAssignment(string keyName)
    {
        StartCoroutine(AssignKey(keyName));
    }

    public void SendText(Text text)
    {
        buttonText = text;
    }

    IEnumerator WaitForKey()
    {
        while (!keyEvent.isKey)
            yield return null;
    }

    public IEnumerator AssignKey(string keyName)
    {
        waitingForKey = true;

        yield return WaitForKey();

        input.SetKeyMap(keyName, newKey);
        buttonText.text = input.GetKeyCode(keyName).ToString();
    }
}
