﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioController : MonoBehaviour {

    public Slider MainSlider, MusicSlider, EffectsSlider;

    public AudioSource MainVolume, MusicVolume, EffectsVolume;

    public bool muteStatus;//true means not muted

	// Use this for initialization
	void Start ()
    {
        MainSlider.value = 1.0f;
        MusicSlider.value = 1.0f;
        EffectsSlider.value = 1.0f;

        muteStatus = true;
    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void MuteButtonClick()
    {
        if (MusicSlider.enabled == true && MainSlider.enabled == true && EffectsSlider.enabled == true)
        {
            MusicVolume.volume = 0.0f; EffectsVolume.volume = 0.0f;

            MusicSlider.enabled = false; MainSlider.enabled = false; EffectsSlider.enabled = false;

            muteStatus = false;
        }
        else
        {
            MusicSlider.enabled = true; MainSlider.enabled = true; EffectsSlider.enabled = true;

            MusicVolume.volume = MusicSlider.value;
            EffectsVolume.volume = EffectsSlider.value;

            muteStatus = true;
        }
    }

   // public void MainVolumeController()
   // {
   //     
   // }
}
