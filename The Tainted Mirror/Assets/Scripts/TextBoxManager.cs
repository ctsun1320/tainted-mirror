﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextBoxManager : MonoBehaviour {

    public GameObject textBox;

    public UIHandler UI;

    public Text theText;

    public Image Protag;
    public Image Antag;

    public Material Default;

    public Sprite pNeutral;
    public Sprite pConfused;
    public Sprite pClosed;
    public Sprite pScared;
    public Sprite pSurprised;

    public Sprite aEvil;
    public Sprite aNeutral;
    public Sprite aSmile;

    public TextAsset textFile;
    public string[] textLines;

    public int currentLine;
    public int endAtLine;

    public float typeSpeed;

    public PlayerController player;

    public bool isActive;

    private bool isTyping = false;
    private bool cancelTyping = false;


    // Use this for initialization
    void Start()
    {

        player = FindObjectOfType<PlayerController>();

        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }

        if (endAtLine == 0)
        {
            endAtLine = textLines.Length - 1;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (UI.wasActivated == false)
        {
            UI.DialougeUIHandler();
            textBox.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        //theText.text = textLines[currentLine];

        if(!isActive)
        {
            return;
        }
        
        if(Input.GetKeyDown(KeyCode.E) && isActive)
        {
            if(!isTyping)
            {
                currentLine += 1;

                if (currentLine > endAtLine)
                {
                    theText.text = "";
                    UI.DialougeUIHandler();
                    UI.wasActivated = true;
                    isActive = false;
                    currentLine = 0;
                    Protag.material = Default;
                    Antag.material = Default;
                }
                else
                {
                    if (textLines[currentLine] == "(Protag_Neutral)\r")
                    {
                        Antag.material = Default;
                        Protag.sprite = pNeutral;
                        Protag.material = null;
                        currentLine += 1;

                    }
                    else if(textLines[currentLine] == "(Protag_Confused)\r")
                    {
                        Antag.material = Default;
                        Protag.sprite = pConfused;
                        Protag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Protag_Closed)\r")
                    {
                        Antag.material = Default;
                        Protag.sprite = pClosed;
                        Protag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Protag_Scared)\r")
                    {
                        Antag.material = Default;
                        Protag.sprite = pScared;
                        Protag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Protag_Surprised)\r")
                    {
                        Antag.material = Default;
                        Protag.sprite = pSurprised;
                        Protag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Antag_Neutral)\r")
                    {
                        Protag.material = Default;
                        Antag.sprite = aNeutral;
                        Antag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Antag_Evil)\r")
                    {
                        Protag.material = Default;
                        Antag.sprite = aEvil;
                        Antag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Antag_Smile)\r")
                    {
                        Protag.material = Default;
                        Antag.sprite = aSmile;
                        Antag.material = null;
                        currentLine += 1;

                    }
                    else if (textLines[currentLine] == "(Exit_Game)")
                    {
                        Destroy(UI.gameObject);
                        SceneManager.LoadScene("WinScene");
                    }
                        StartCoroutine(TextScroll(textLines[currentLine]));
                }
            }
            else if (isTyping && !cancelTyping)
            {
                cancelTyping = true;
            }
        }
	}

    private IEnumerator TextScroll(string lineOfText)
    {
        int letter = 0;
        theText.text = "";
        isTyping = true;
        cancelTyping = false;
        while (isTyping && !cancelTyping && (letter < lineOfText.Length - 1))
        {
            theText.text += lineOfText[letter];
            letter += 1;
            yield return new WaitForSeconds(typeSpeed);
        }
        theText.text = lineOfText;
        isTyping = false;
        cancelTyping = false;
    }
}
