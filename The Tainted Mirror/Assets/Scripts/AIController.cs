﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

	//private float walkSpeed, runSpeed, sprintAccel, walkAccel;
    private float jumpPower, moveSpeed,  moveAccel, speedOffset;
    [SerializeField] private GameObject player;
    [SerializeField] private UIHandler UIMenu;

    [SerializeField] private Rigidbody rb; 
    //[SerializeField] private CharacterController controller;
    //public float gravity;
    private EnemySpawner enemySpawn;
    // might use Player2Controller and have it work with CharacterControllers instead - runs even funnier

    private bool playerToRight, facingRight, isGrounded, hasJumped;

	// Use this for initialization
	void Start () {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");
        if (UIMenu == null)
            UIMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();
        if (enemySpawn == null)
            enemySpawn = UIMenu.enemyTriggers[UIMenu.level].GetComponent<EnemySpawner>();
        //if using characterController
        //controller = GetComponent<CharacterController>();
        //walkSpeed = 2.0F;
        //runSpeed = 3.0F;
        //jumpPower = 8.0F; 
        //gravity = 20.0F;

        //if using rigidbody //-0.25f from player's
        rb = GetComponent<Rigidbody>();
        //sprintAccel = 70.0f;
        //walkAccel = 50.0f;
		//runSpeed = 2.0f;
        //walkSpeed = 1.0f;
		
        jumpPower = 5.0f;

        speedOffset = -0.32f;

        facingRight = false;
        playerToRight = false;
    }
	
	// Update is called once per frame
	void Update () {
		if (!UIMenu.isPaused)
        {
            Vector3 moveForceX = new Vector3();
            Vector3 moveForceY = new Vector3();
            //if using rb
            float curSpeed = rb.velocity.x;
            float curYSpeed = rb.velocity.y;

            if (player.transform.position.x > transform.position.x + 0.5*Mathf.Abs(transform.localScale.x))
            {
                moveForceX.x = 1.0f;
                playerToRight = true;
            }

            else if (player.transform.position.x < transform.position.x - 0.5 * Mathf.Abs(transform.localScale.x))
            {
                moveForceX.x = -1.0f;
                playerToRight = false;
            }
            else
                moveForceX.x = 0.0f;

            if (player.transform.position.y > transform.position.y + 0.5 * Mathf.Abs(transform.localScale.y))
            {
                moveForceY.y = 1.0f;
            }
            else if (player.transform.position.y < transform.position.y - 0.5 * Mathf.Abs(transform.localScale.y))
            {
                moveForceY.y = -1.0f;
            }
            else
                moveForceY.y = 1.0f;

            if (playerToRight && !facingRight ||
                !playerToRight && facingRight)
            {
                facingRight = !facingRight;

                Vector3 scale = transform.localScale;
                scale.x *= -1;
                transform.localScale = scale;
            } //flip code
            

			
			moveAccel = player.GetComponent<PlayerController>().moveAccel;
            moveSpeed = player.GetComponent<PlayerController>().moveSpeed - speedOffset;

            //if (player.GetComponent<PlayerController>().isRunning)
            //{
            //    moveAccel = sprintAccel;
            //    moveSpeed = runSpeed - speedOffset;
            //}
            //else
            //{
            //    moveAccel = walkAccel;
            //    moveSpeed = walkSpeed - speedOffset;
            //}

			
            //if using character controller
            //if (controller.isGrounded)
            //{
            //    hasJumped = false;

            //}
            //else if (!controller.isGrounded && hasJumped == false)
            //{
            //    moveForce.y = jumpPower;
            //    hasJumped = true;
            //}

            //moveForce.y -= gravity * Time.deltaTime;
            //controller.Move(moveForce * Time.deltaTime);

            //if using rb
            if (rb.velocity.y > -0.001 && rb.velocity.y < 0.001)
            {
                hasJumped = false;
                isGrounded = true;
            }
            else
            {
                isGrounded = false;
            }

            rb.velocity = new Vector3(moveForceX.x * moveSpeed, moveForceY.y * moveSpeed, 0);

            //if (curSpeed < moveSpeed && curSpeed > -moveSpeed)
            //{
            //    rb.AddForce(moveForceX * moveAccel);
            //}
            //if (curYSpeed < moveSpeed && curYSpeed > -moveSpeed)
            //{
            //    rb.AddForce(moveForceY * moveAccel);
            //}

            //if (!isGrounded && !hasJumped)
            //{
            //    if (facingRight)
            //    {
            //        rb.velocity = new Vector3(2.0f, jumpPower * 0.8f, 0);
            //    }
            //    else
            //        rb.velocity = new Vector3(-2.0f, jumpPower * 0.8f, 0);

            //    hasJumped = true;
            //}
        }

    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.collider.tag == "Player")
    //    {
    //        PlayerController player = collision.collider.GetComponent<PlayerController>();

    //        //player.transform.position = player.RespawnPos;
    //        //player.respawned = true;
    //        UIMenu.isPaused = true;
    //        Cursor.visible = true;
    //        Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
    //        UIMenu.DeathScreen.enabled = true;
    //        player.willDie = true;
    //        //Destroy(this.gameObject); //will be done on respawn screen - moved there to avoid enemies still on screen if you die by other means
    //        //enemySpawn.enemySpawned = false; //moved to rspawn screen - similar reasons - re-enable it to spawn enemies if you die by other means

    //        GameObject[] fallingPlatforms = GameObject.FindGameObjectsWithTag("Falling");
    //        foreach (GameObject obj in fallingPlatforms)
    //        {
    //            obj.GetComponent<PlatformController>().ResetPlat(); //resets every timer in all falling platforms if player dies.
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider other) //moved it here to avoid it getting stuck on platforms
    {
        if (other.tag == "EnemyDestroy")
        {
            Destroy(this.gameObject);
            enemySpawn.enemyPassed = true;
        }

        if (other.tag == "Player")
        {
            PlayerController player = other.GetComponent<PlayerController>();

            //player.transform.position = player.RespawnPos;
            //player.respawned = true;
            UIMenu.isPaused = true;
            Cursor.visible = true;
            Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
            UIMenu.DeathScreen.enabled = true;
            player.willDie = true;
            //Destroy(this.gameObject); //will be done on respawn screen - moved there to avoid enemies still on screen if you die by other means
            //enemySpawn.enemySpawned = false; //moved to rspawn screen - similar reasons - re-enable it to spawn enemies if you die by other means

            GameObject[] fallingPlatforms = GameObject.FindGameObjectsWithTag("Falling");
            foreach (GameObject obj in fallingPlatforms)
            {
                obj.GetComponent<PlatformController>().ResetPlat(); //resets every timer in all falling platforms if player dies.
            }
        }

    }
}
