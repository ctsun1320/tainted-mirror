﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public UIHandler uiMenu;
    public bool enemySpawned;
    public bool enemyPassed;

    void Start()
    {
        if (uiMenu == null)
            uiMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();

        enemySpawned = false;
        enemyPassed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!enemySpawned && !enemyPassed)
            {
                GameObject bossSpawn = GameObject.Instantiate(Resources.Load("Enemy")) as GameObject;
                bossSpawn.transform.position = uiMenu.enemySpawnSpots[uiMenu.level].position;
                enemySpawned = true;
            }

        }
    }
}
