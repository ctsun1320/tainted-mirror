﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float sprintAccel = 70.0f;
    [SerializeField] private float walkAccel = 50.0f;
    [SerializeField] private float jumpPower = 5.0f;
    [SerializeField] private float runSpeed = 2.0f;
    [SerializeField] private float walkSpeed = 1.0f;
    public float climbSpeed = 1.3f;
    public float climbAccel = 50.0f;

   [SerializeField] public Rigidbody rb;

    public float moveSpeed, moveAccel, yMove;
    private float curSpeed;

    [SerializeField] private Animator animator;
    private bool facingRight, isCrouching, canClimb;
    public bool isRunning, isGrounded, willDie, isCorrected, respawned, isJumping;
    public bool JumpKeyPressed;

    [SerializeField] private UIHandler UIMenu;
    [SerializeField] private InputManager input;
    public Vector3 RespawnPos;

    public List<KeyData> keys = new List<KeyData>();

    //private Vector3 crouchScale, normalScale;

    private Vector3 colliderSize;

    private void Awake()
    {
        if (UIMenu == null)
            UIMenu = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<UIHandler>();
        if (input == null)
            input = GameObject.FindGameObjectWithTag("UIMenu").GetComponent<InputManager>();
    }

    // Use this for initialization
    void Start()
    {
        if (rb == null)
            rb = GetComponent<Rigidbody>();
        if (animator == null)
            animator = GetComponent<Animator>();



        facingRight = true;
        willDie = false;
        isCorrected = false;
        respawned = false;
        isJumping = false;
        JumpKeyPressed = false;

        //normalScale = this.transform.localScale;

        //crouchScale.x = this.transform.localScale.x * 2.0f;
        //crouchScale.y = this.transform.localScale.y * 0.5f;

        colliderSize = this.gameObject.GetComponent<BoxCollider>().size;

        RespawnPos = rb.position;

    }

    void Update()
    {
        if (!UIMenu.isPaused && UIMenu.DeathScreen.enabled != true)
        {
            Cursor.visible = false;
            if (input.GetKeyDown("Jump") && !isCrouching && !canClimb && isGrounded)
            {
                JumpKeyPressed = true;
            }
        }
        else if (UIMenu.isPaused == true)
        {
            Cursor.visible = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!UIMenu.isPaused && UIMenu.DeathScreen.enabled != true)
        {
            Vector3 inputForce = new Vector3();
            curSpeed = rb.velocity.x;
            inputForce.x = GetHorAxes();
            yMove = GetVertAxis();

            Flip(inputForce.x);

            if (rb.velocity.y > -0.009 && rb.velocity.y < 0.009)
            {
                animator.SetBool("isJumping", false);
                isGrounded = true;
                //isJumping = false;
            }
            else
            {
                isGrounded = false;
            }

            if (inputForce.x != 0)
            {
                animator.SetInteger("Direction", 1);
                //if (input.GetKey("Run") == true && !isCrouching && isGrounded)
                //{
                if (isCrouching)
                {
                    isRunning = false;
                    moveAccel = walkAccel;
                    moveSpeed = walkSpeed;
                    animator.SetBool("isRunning", false);
                    animator.SetBool("Crouching", true);
                }
                else if (animator.GetBool("isClimbing") == false)
                {
                    isRunning = true; //will be looked at by ai
                    moveAccel = sprintAccel;
                    moveSpeed = runSpeed;
                    animator.SetBool("isRunning", true);
                }
                //}
                //else
                //{
                //    isRunning = false;
                //    moveAccel = walkAccel;
                //    moveSpeed = walkSpeed;
                //    animator.SetBool("isRunning", false);
                //}
            }
            else
            {
                isRunning = false;
                animator.SetInteger("Direction", 0);
                animator.SetBool("isRunning", false);
            }

            if (isGrounded)
            {
                if (JumpKeyPressed && !isCrouching && !canClimb)
                {
                    if (isRunning)
                    {
                        if (facingRight)
                        {
                            rb.velocity = new Vector3(moveSpeed * 1.0f, jumpPower, 0);
                            //rb.AddForce(0, jumpPower * 0.8f, 0, ForceMode.Impulse);
                        }
                        else
                            rb.velocity = new Vector3(moveSpeed * -1.0f, jumpPower, 0);
                        //rb.AddForce(0, jumpPower * 0.8f, 0, ForceMode.Impulse);

                    }
                    //else if (canClimb)
                    //{
                    //    rb.velocity = new Vector3(0, jumpPower * 0.5f, 0); //decrease jump's power a bit in case of ladders
                    //    rb.useGravity = true;
                    //}
                    else if (!isRunning && inputForce.x != 0)
                    {
                        if (facingRight)
                        {
                            rb.velocity = new Vector3(moveSpeed, jumpPower, 0);
                        }
                        else
                        {
                            rb.velocity = new Vector3(-moveSpeed, jumpPower, 0);
                        }
                    }
                    else if (inputForce.x == 0)
                    {
                        rb.velocity = new Vector3(0, jumpPower, 0);
                    }

                    isJumping = true;
                    animator.SetBool("isJumping", true);
                    animator.SetBool("isClimbing", false);
                }

                

                if (curSpeed < moveSpeed && curSpeed > -moveSpeed && !isJumping)
                {
                    rb.AddForce(inputForce * moveAccel);// accelerating up to movement speed
                }
                else if (curSpeed >= moveSpeed && !isJumping)
                {
                    rb.velocity = new Vector3(moveSpeed, rb.velocity.y, rb.velocity.z);// sets speed to movement speed
                }
                else if (curSpeed <= -moveSpeed && !isJumping)
                {
                    rb.velocity = new Vector3(-moveSpeed, rb.velocity.y, rb.velocity.z);// does the same to opposite direction
                }


                if (input.GetKey("Crouch") && animator.GetBool("isClimbing") == false)
                {
                    animator.SetBool("Crouching", true);
                    isCrouching = true;
                    this.transform.GetComponent<BoxCollider>().size = new Vector3(colliderSize.x * 1.4f, colliderSize.y * 0.72f, 0);
                    if (rb.velocity.x != 0)
                    {
                        animator.SetBool("isCrawling", true);
                    }
                    else
                    {
                        animator.SetBool("isCrawling", false);
                    }
                    //transform.localScale = crouchScale;
                    float curPosX = rb.position.x;
                    float newPosY = rb.position.y - 0.25f;
                    if (isCorrected == false)
                    {
                        Vector3 newPos = new Vector3(curPosX, newPosY, 0);
                        rb.MovePosition(newPos);
                        isCorrected = true;
                    }
                }
                else
                {
                    //transform.localScale = normalScale;
                    this.transform.GetComponent<BoxCollider>().size = colliderSize;
                    isCorrected = false;
                    isCrouching = false;
                    animator.SetBool("Crouching", false);
                    animator.SetBool("isCrawling", false);
                }

            }

            if (rb.velocity.y <= -10)
                willDie = true;

            if (animator.GetBool("isClimbing") == false && animator.enabled == false)
            {
                animator.enabled = true; //climbing and crouching has special cases but run animator otherwise
            }

        }
        else
        {
            if (animator.enabled == true)
                animator.enabled = false;
        }

    }

    private void Flip(float xMove)
    {
        if (xMove > 0 && !facingRight || xMove < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 scale = transform.localScale;
            //crouchScale.x *= -1;
            scale.x *= -1;
            //normalScale.x *= -1;
            transform.localScale = scale;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ladder")
        {
            //float yMove = GetVertAxis();

            if (!UIMenu.isPaused)
            {
                //if (rb.velocity.y <= 0)
                if (this.transform.position.y > other.transform.position.y + 0.5*other.transform.localScale.y)
                {

                    rb.velocity = new Vector3(rb.velocity.x, 0, 0); //still tends to fall off ladders
                    rb.useGravity = false;
                }
				
            }

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Ladder")
        {
            if (!UIMenu.isPaused)
            {
                float yMove = GetVertAxis();

                //if (yMove < 0 && rb.velocity.y < climbSpeed && animator.GetBool("isClimbing") == true)
                //{
                //    rb.AddForce(0, climbAccel, 0);
                //}
                //if (yMove != 0)
                //    animator.SetBool("isClimbing", true);
                if (!isCrouching && !isRunning) //rb.velocity.x == 0 &&
                {
                    if (yMove != 0 && isGrounded && animator.GetBool("isClimbing") == false)
                    {
                        if (animator.GetBool("isRunning") == true)
                            animator.SetBool("isRunning", false);

                        //animator.SetBool("isJumping", false);
                        animator.SetBool("isClimbing", true);
                        rb.velocity = new Vector3(rb.velocity.x, 0, 0);
                        rb.useGravity = false; //start climbing ladder, we don't want gravity interfering
                        isGrounded = false;
						canClimb = true;
                    }
                    if (animator.GetBool("isClimbing") == true)
                    {
                        if (yMove == 0)
                        {
                            //if is climbing and no movement either way
                            //rb.velocity = new Vector3(0, 0, 0);
                            if (animator.enabled == true)
                                animator.enabled = false; //pause animation
                        }
                        else
                        {
                            //if (rb.velocity.y < climbSpeed && rb.velocity.y > -climbSpeed)
                            //{
                            //    rb.AddForce(0, climbAccel, 0);
                            //}
                            //else if (rb.velocity.y >= climbSpeed)
                            //{
                            //    rb.velocity = new Vector3(0, climbSpeed, 0);
                            //}
                            //else if (rb.velocity.y <= -climbSpeed)
                            //{
                            //    rb.velocity = new Vector3(0, -climbSpeed, 0);
                            //}

                            rb.position = new Vector3(rb.position.x, rb.position.y + (yMove * climbSpeed *Time.deltaTime), rb.position.z);
                            if (animator.enabled == false)
                                animator.enabled = true;
                        }

                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Ladder")
        {
            isGrounded = true;
            rb.useGravity = true; //go back to using gravity again
            animator.SetBool("isClimbing", false);
            if (animator.enabled == false)
                animator.enabled = true; //just in case
            canClimb = false;
        }
    }

    public void AddKey(KeyData key)
    {
        key.keyColor = key.shardSprite.color;
        keys.Add(key);
        key.image.color = key.keyColor;
        //Color newColor = ;
    }

    public void RemoveKeys()
    {
        foreach (var keys in keys)
        {
            keys.image.color = Color.black;
        }
        keys.Clear();
    }

    public bool HasKeyType(KeyData.EType type)
    {
        int numKeysOfType = keys.Where(z => z.type == type).Count();

        return numKeysOfType > 0;
    }

    public int GetKeyNum()
    {
        return keys.Count;
    }

    private void OnCollisionEnter(Collision collision)
    {
        isJumping = false;
        JumpKeyPressed = false;

        if (willDie == true)
        {
            //rb.MovePosition(RespawnPos);
            UIMenu.DeathScreen.enabled = true;
            Cursor.visible = true;
            Time.timeScale = Mathf.Abs(Time.timeScale - 1.0f);
            UIMenu.isPaused = !UIMenu.isPaused;

            GameObject[] fallingPlatforms = GameObject.FindGameObjectsWithTag("Falling");
            foreach (GameObject obj in fallingPlatforms)
            {
                obj.GetComponent<PlatformController>().ResetPlat(); //resets every timer in all falling platforms if player dies.
            }
        }
    }

    public float GetHorAxes()
    {
        if (input.GetKey("Left") || input.GetKey("Alt Left"))
        {
            return -1.0f;
        }
        else if (input.GetKey("Right") || input.GetKey("Alt Right"))
        {
            return 1.0f;
        }
        else
            return 0.0f;
    }

    private float GetVertAxis()
    {
        if (input.GetKey("Down") || input.GetKey("Alt Down"))
        {
            return -1.0f;
        }
        else if (input.GetKey("Up") || input.GetKey("Alt Up"))
        {
            return 1.0f;
        }
        else
            return 0;
    }

}