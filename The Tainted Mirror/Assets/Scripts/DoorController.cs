﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{

    //protected float KeyDown;

    protected GameObject Menu;

    public int KeysInLevel;

    private void Awake()
    {
        if (Menu == null)
            Menu = GameObject.FindGameObjectWithTag("UIMenu");
    }

    void Update()
    {
        //KeyDown = Input.GetAxis("Interact");

    }

    void OnTriggerEnter(Collider other)
    {
        PlayerController player = other.GetComponent<PlayerController>();
        //Player2Controller player = other.GetComponent<Player2Controller>();
        int NumofKeys = player.GetKeyNum();
            if (player != null)
            {
            if (NumofKeys == KeysInLevel)
            {
                   player.RemoveKeys();
                    //SceneManager.LoadScene("2nd Level WIP");
                    Menu.GetComponent<UIHandler>().level += 1;
                    if (Menu.GetComponent<UIHandler>().level >= Menu.GetComponent<UIHandler>().spawnSpots.Length) //this absolutely needs spawnSpots defined in UI to get the right order
                    {
                    player.transform.position = Menu.GetComponent<UIHandler>().spawnSpots[Menu.GetComponent<UIHandler>().spawnSpots.Length-1].position;
                    }
                    else
                    {
                         player.transform.position = Menu.GetComponent<UIHandler>().spawnSpots[Menu.GetComponent<UIHandler>().level].position;
                    }
					player.RespawnPos = player.transform.position;

            }
            if (this.tag == "Exit")
            {
                Destroy(Menu.gameObject);
                SceneManager.LoadScene("WinScene");
            }
        }
    }
}